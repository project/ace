; Ace Editor

api = 2
core = 7.x

libraries[ace][download][type] = "get"
libraries[ace][download][url] = "https://github.com/ajaxorg/ace-builds/archive/v1.4.1.zip"
